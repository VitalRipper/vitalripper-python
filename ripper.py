import os
import sys
import requests
import lxml.html
from fpdf import FPDF
from PIL import Image


def get_session(email, password, book):
    s = requests.session()

    # Get the login page and html
    login_url = 'https://jigsaw.vitalsource.com/login?return=/books/{}/content/image/1.jpg'.format(book)
    login = s.get(login_url)
    login_html = lxml.html.fromstring(login.text)

    #retrieve hidden inputs from the form
    hidden_inputs = login_html.xpath(r'//form//input[@type="hidden"]')
    form = {x.attrib["name"]: x.attrib["value"] for x in hidden_inputs}

    #add login details to the form
    form['user[email]'] = email
    form['user[password]'] = password

    #get response
    response = s.post(login_url, data=form)

    return s


def convert_images_to_pdf(book):

    #directory of images
    directory = "images\\{}\\".format(book)

    #get pdf size from the first page
    first_page = Image.open("images\\{}\\0001.jpg".format(book))
    width, height = first_page.size

    #set pdf units and width and height
    pdf = FPDF(unit = "pt", format=[width, height])


    d = os.fsencode(directory)

    #iterate over everyfile and add to pdf
    for file in os.listdir(d):
        filename = os.fsdecode(file)
        pdf.add_page()
        pdf.image("{}{}".format(directory, filename), 0, 0)

    #print out the pdf. This can take some time
    print("Compiling PDF. This may take several minutes.")
    pdf.output("output\\{}.pdf".format(book), "F")
    print("Thanks for using VitalRipper! Your book can be found in the output folder.")


def download_book(email, password, book):
    
    #Get the session cookies
    session = get_session(email, password, book)

    #set loop variables
    valid_response = True
    page = 1

    #make the directory if it does not exist
    if not os.path.exists("images\\{}".format(book)):
        print("Directory doesn't exist. Creating it.")
        os.makedirs("images\\{}".format(book))

    print("We are now downloading all the images. " 
        "Depending on book length this may take some time.")
    #loop through and download all pages. Will stop once the next page doesn't exist
    while valid_response:

        #print page info to keep user engaged
        if page % 10 == 0:
            print("Downloading page {}".format(page))
        
        #url to image
        url = "https://jigsaw.vitalsource.com/books/{}/content/image/{}.jpg".format(book, page)
        
        #download image
        image = session.get(url)

        #check if status code ok
        if image.status_code == requests.codes.ok:

            #write image to file. This code found from "automate the boring stuff"
            image_file = open(os.path.join('images\\{}'.format(book), "{}.jpg".format(str(page).zfill(4))), 'wb')
            for chunk in image.iter_content(100000):
                image_file.write(chunk)
            image_file.close()
            page += 1
        else:
            #stop loop
            if page == 1:
                print("ERROR: This book could not be downloaded." 
                        "This file may not be a PDF. Non-PDF downloads coming soon!")
            else:
                print("End of pages. Book is {} pages long".format(page))
                valid_response = False
                #Convert all images to pdf
                convert_images_to_pdf(book)





if __name__ == "__main__":
    if len(sys.argv) == 4:
        print("Welcome to VitalRipper! Just doing some preliminary setup!")
        email = sys.argv[1]
        password = sys.argv[2]
        book = sys.argv[3]
        download_book(email, password, book)
    else:
        print("Usage: python ripper.py email password book_number")