@echo off

REM Check directory folders exist and make ones that don't

if not exist output mkdir output
if not exist images mkdir images

REM Install all libraries needed

pip install requests fpdf lxml pillow

REM Request information from users

SET /P email=Please enter your email: 
SET /P pass=Please enter your password: 
SET /P book=Please enter the book number: 

python.exe ripper.py %email% %pass% %book%